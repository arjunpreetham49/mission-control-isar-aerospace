import { setLatestData, store } from "../web/redux/apiRedux";

var client;

var connectionFlag = false; 

export const toggleLiveTelemetry = (init) => {
    if(init){
        client = new WebSocket('wss://isaraerospace-webdeveloper-assignment.azurewebsites.net/api/SpectrumWS?token=0DB9D71DE67');

        client.onopen = () => {
            console.log("CONNECTION STATUS: CONNECTED");
        }

        client.onclose = () => {
            console.log("CONNECTION STATUS: DISCONNECTED");
        }

        client.onerror = (err) => {
            console.log("WS ERROR: ", err);
        }

        client.onmessage = (e) => {
            const wsData = JSON.parse(e.data); 

            const formatedData = {
                velocity: {
                    x: wsData.Velocity.X,
                    y: wsData.Velocity.Y,
                    z: wsData.Velocity.Z,
                },
                altitude: wsData.Altitude,
                temperature: wsData.Temperature,
                goingUp: wsData.GoingUp,
                statusMessage: wsData.StatusMessage
            }

            store.dispatch(setLatestData({data: formatedData}));
        }

    }else{
        client.close(); 
    }
}
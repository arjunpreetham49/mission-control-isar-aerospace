import axios from 'axios';
import { setLatestData } from '../web/redux/apiRedux';
import { store } from '../web/redux/apiRedux';

//API Details
const baseUrl = 'https://isaraerospace-webdeveloper-assignment.azurewebsites.net/api';
const token = "0DB9D71DE67"; 


export const changeDirection = () => {
    let storeDirection = store.getState().graphData.direction; 

    let change = storeDirection !== null ? !storeDirection : false; 
    console.log(change);
    console.log(!storeDirection);
    console.log(store.getState().graphData);

    axios.get(`${baseUrl}/SpectrumChangeDirection?token=${token}&direction=${change}`).then((response) => {
        getLatestData(); 
    });    
}

export const getLatestData = () => {
    axios.get(`${baseUrl}/SpectrumStatus?token=${token}`).then((response) => {
        console.log(response.data);
        
        store.dispatch(setLatestData({data: response.data}));
    });
}
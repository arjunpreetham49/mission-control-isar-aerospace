import { registerRootComponent } from 'expo';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import { AppRegistry } from 'react-native';

import App from './App';
import { store } from './web/redux/apiRedux';

// registerRootComponent calls AppRegistry.registerComponent('main', () => App);
// It also ensures that whether you load the app in Expo Go or in a native build,
// the environment is set up appropriately
// registerRootComponent(App);

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
)

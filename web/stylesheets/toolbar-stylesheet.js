import { StyleSheet } from 'react-native';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000000',
        minHeight: "100vh",
        minWidth: "100vh",
    },

    toolbar: {        
        flex: 1, 
        flexDirection: "row",
        minWidth: "100vh",
        justifyContent: 'space-evenly'
    },

    //Mission Control Logo & Text
    mcStyle: {
        paddingTop: "3vh",
        paddingStart: "8vh",
        flexDirection:'row', 
        justifyContent: 'space-between',
        alignSelf: "baseline"
    },
    logo: {
        width: "5vh",
        height: "5vh",
        resizeMode: "contain",
        position: "absolute",
        top: "2vh",
        left: "1.5vh"
    },
    mcTextStyle: {
        color: "#FFFFFF",
        fontSize: "2vh",
        fontWeight: "bold"
    },

    //Live Mode Control Styles
    
    liveModeStyle: {
        paddingTop: "3vh",
        paddingEnd: "4vh",
        flexDirection:'row', 
        justifyContent: 'flex-end',
    },
    liveModeTextStyleEnabled: {
        color: "#27a838",
        fontSize: "2vh",
        fontWeight: "bold",
        borderColor: "#27a838",
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: "0.8vh",
        paddingRight: "0.8vh",
        paddingTop: "0.4vh",
        paddingBottom: "0.5vh",
    },
    liveModeTextStyleDisabled: {
        color: "#bf3636",
        fontSize: "2vh",
        fontWeight: "bold",
        borderColor: "#bf3636",
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: "0.8vh",
        paddingRight: "0.8vh",
        paddingTop: "0.4vh",
        paddingBottom: "0.5vh",
    },

    //UTC TimeStamp Style
    utcTimeStamp: {
        color: "#FFFFFF",
        fontSize: "2vh",
        textAlign: "center",
        paddingTop: "2.8vh"
    },

    //Toolbar
    toolbar_part:{
        flex: 1,
    }
});

export default styles;




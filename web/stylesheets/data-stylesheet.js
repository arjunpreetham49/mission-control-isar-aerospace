import { StyleSheet } from 'react-native';

const dataStyles = StyleSheet.create({
    container: {
        flex: 8,
        backgroundColor: '#000000',
        height: "100%",
        width: "100%"
    },

    //Acending/Descending Text Style
    directionAscending: {
        textAlign: "center",
        paddingTop: "3vh",
        fontSize: "2vh",
        fontWeight: "bold",
        backgroundColor: "#27a838",
        color: "#FFFFFF",
        borderColor: "#27a838",
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: "6vh",
        paddingRight: "6vh",
        paddingTop: "0.4vh",
        paddingBottom: "0.5vh",
        alignSelf: "center",
    },
    directionDescending: {
        textAlign: "center",
        paddingTop: "3vh",
        fontSize: "2vh",
        fontWeight: "bold",
        backgroundColor: "#bf3636",
        color: "#FFFFFF",
        borderColor: "#bf3636",
        borderWidth: 1,
        borderRadius: 5,
        paddingLeft: "6vh",
        paddingRight: "6vh",
        paddingTop: "0.4vh",
        paddingBottom: "0.5vh",
        alignSelf: "center",
    },

    //Graph container 
    graphContainer: {
        justifyContent: 'center',
        flexDirection: 'row',
        alignItems: 'flex-start',
        top: "5vh"
    },

    graphLabel: {
        color: "#FFFFFF",
        textAlign: "center",
        fontWeight: "bold",
        fontSize: "1.15em",
        padding: 10
    },

    //Message Log Styles
    logMessageStyle:{
        color: "#FFFFFF",
    },

    logHeading: {
        color: "#FFFFFF",
        textAlign: "center",
        fontWeight: "bold",
        fontSize: "1.15em",
        backgroundColor: "#e8be27",
        borderTopEndRadius: 8,
        borderTopStartRadius: 8
    },

    listContainer: {
        backgroundColor: "#3f4147",
        left: "2vh",
        flexDirection: 'column',
        borderRadius: 8,
        height: "100%",
        width: "70%"
    },

    listStyle: {
        padding: 8
    },

    //Control Panel Styles
    cpContainer: {
        backgroundColor: "#3f4147",
        left: "5vh",
        flexDirection: 'column',
        borderRadius: 8,
        height: "100%",
        width: "25%"
    },

    cpHeading: {
        backgroundColor: "#62b043",
        borderTopLeftRadius: 8,
        borderTopEndRadius: 8,
        color: "#FFFFFF",
        textAlign: "center",
        fontWeight: "bold",
        fontSize: "1.15em",
    },

    cpButton: {
        top: 10,
        left: 10, 
        right: 10,
        width: "80%",
        alignSelf: "center"
    },

    cpButton2: {
        top: 30,
        left: 10, 
        right: 10,
        width: "80%",
        alignSelf: "center"
    },

    cpButtonText: {
        textAlign: 'center',
        color: "#FFFFFF"
    }
});

export default dataStyles;




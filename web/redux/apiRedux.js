import { configureStore, createSlice, combineReducers, current  } from '@reduxjs/toolkit';

//Reducers
export const liveModeSlice = createSlice({
    name: 'liveMode',
    initialState: {
        liveMode: true
    },
    reducers: {
      setLiveMode: (state, action) => {
        state.liveMode = action.payload.liveMode
      }
    }
});

export const dataSlice = createSlice({
    name: 'latestData',
    initialState: {
        altitude: [],
        direction: null, 
        prevDirection: null,
        statusMessage: [],
        temperature: [], 
        velocity: []
    },
    reducers: {
      setLatestData: (state, action) => {
        let newData = action.payload.data;

        let storeData = current(state); 
        
        let storeAltitude = Object.assign(storeData.altitude.length > 0 ? storeData.altitude : [], storeAltitude);  
        let storeTemp = Object.assign(storeData.temperature.length > 0 ? storeData.temperature : [], storeTemp);   
        let storeVelocity = Object.assign(storeData.velocity.length > 0 ? storeData.velocity : [], storeVelocity); 
        let storeDirection = storeData.direction; 
        let storeStatusMessages = Object.assign(storeData.statusMessage.length > 0 ? storeData.statusMessage : [], storeStatusMessages);     

        //Appending altitude data to store
        const newAlt = [...storeAltitude];
        newAlt.push(newData.altitude);
        storeAltitude = newAlt;

        //Appending temperature data to store
        const newTemp = [...storeTemp];
        newTemp.push(newData.temperature);
        storeTemp = newTemp;

        //Formating velocity data & appending to store
        const xVel = newData.velocity.x;
        const yVel = newData.velocity.y; 
        const zVel = newData.velocity.z; 
        const normalizedVel = Math.sqrt(Math.pow(xVel, 2) + Math.pow(yVel, 2) + Math.pow(zVel, 2));

        const newVelocity = {
          x: xVel,
          y: yVel,
          z: zVel, 
          norm: normalizedVel !== null ? normalizedVel : 0
        }

        const newVelocityArr = [...storeVelocity];
        newVelocityArr.push(newVelocity);
        storeVelocity = newVelocityArr;

        //Formating status message to include statusMessage from response & vehicle direction message
        const statMsg = {
          timestamp: new Date().toISOString(),
          message: newData.statusMessage
        }
        storeStatusMessages = [...storeStatusMessages, statMsg];

        if(storeDirection !== newData.goingUp){
          const directionMsg = {
            timestamp: new Date().toISOString(),
            message: newData.goingUp ? "Spectrum Ascending" : "Spectrum Descending"
          }

          storeStatusMessages = [...storeStatusMessages, directionMsg];
        }
        
        state.altitude = storeAltitude;
        state.prevDirection = !newData.goingUp; 
        state.direction = newData.goingUp;
        state.statusMessage = storeStatusMessages;
        state.temperature = storeTemp;
        state.velocity = storeVelocity;
      }
    }
});

export const { setLatestData } = dataSlice.actions;
export const { setLiveMode } = liveModeSlice.actions;

export const store = configureStore({
    reducer: combineReducers({
        // direction: directionSlice.reducer, 
        graphData: dataSlice.reducer,
        uiData: liveModeSlice.reducer
    })
});
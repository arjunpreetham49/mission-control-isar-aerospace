import { Text, View } from 'react-native';
import { useEffect, useState } from "react";
import { store } from '../../redux/apiRedux';
import dataStyles from '../../stylesheets/data-stylesheet';
import { Button } from 'react-native-web';
import {changeDirection, getLatestData} from '../../../api/axios'

export function ControlPanel(){
    const [liveMode, setLiveMode] = useState(false);

    useEffect(() => {
        store.subscribe(() => {
            let state = store.getState().uiData.liveMode; 
            console.log("STATE", state);
            setLiveMode(!state); 
        });
    }, []);

    return(
        <View style={dataStyles.cpContainer}>
            <Text style = {dataStyles.cpHeading}>Control Panel</Text>

            <View style = {dataStyles.cpButton}>
                <Button 
                title = "Change Vehicle Direction"
                color = "#e07924"
                onPress = {() => {
                    changeDirection();
                }} />
            </View>

            <View style = {dataStyles.cpButton2}>
                {liveMode ? (
                    <Button 
                    title = "Refresh Data"
                    color = "#e07924"
                    onPress = {() => {
                        getLatestData(); 
                    }} />
                ) : (
                    <Button 
                    title = "Refresh Data"
                    color = "#e07924"
                    disabled />
                )}
            </View>
        </View>
    );

}
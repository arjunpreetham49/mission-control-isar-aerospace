import { LineChart } from "react-native-chart-kit";
import { Dimensions } from 'react-native-web';
import { Text, View } from 'react-native';
import { useEffect, useState } from "react";
import { store } from '../../redux/apiRedux';
import dataStyles from '../../stylesheets/data-stylesheet';

export function GraphSection(){
    const [velocityData, setVelocity] = useState([]);
    const [tempData, setTemp] = useState([]);
    const [altData, setAltitude] = useState([]); 

    useEffect(() => {
        store.subscribe(() => {
            const storeData = store.getState();
            
            if(storeData !== null && storeData.graphData !== null){
                bindDataToView(storeData.graphData);
            }else{
                //Show state
            }
        });
    }, []);

    // Function that takes data and binds it to the graph
    function bindDataToView(data){
        //Showing only latest 5 values

        //Binding velocity data to graph
        let newVel = data.velocity;

        if(newVel.length > 5){
            let temp = [...newVel];
            newVel = temp.slice(newVel.length - 6, newVel.length - 1);
        }
        
        //Extracting normalized velocity for the graphs
        let normVel = [];
        for(let i = 0; i < newVel.length; i++){
            normVel.push(newVel[i].norm);
        }

        setVelocity(normVel);        

        //Binding temperature data to graph
        let newTemp = data.temperature;

        if(newTemp.length > 5){
            let temp = [...newTemp];
            newTemp = temp.slice(newTemp.length - 6, newTemp.length - 1);
        }

        setTemp(newTemp);

        //Binding altitude
        let newAlt = data.altitude;

        if(newAlt.length > 5){
            let temp = [...newAlt];
            newAlt = temp.slice(newAlt.length - 6, newAlt.length - 1);
        }

        setAltitude(newAlt);
    }

    return(
        <View style={dataStyles.graphContainer}>
            <View >
                <LineChart
                    data={{
                    labels: new Array(velocityData.length),
                    datasets: [
                        {
                        data: velocityData
                        }
                    ]
                    }}
                    width={Dimensions.get("window").width/3.5} // from react-native
                    height={220}
                    yAxisInterval={1} // optional, defaults to 1
                    chartConfig={{
                    backgroundGradientFrom: "#3370d6",
                    backgroundGradientTo: "#3370d6",
                    decimalPlaces: 2, // optional, defaults to 2dp
                    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    style: {
                        borderRadius: 16
                    },
                    propsForDots: {
                        r: "6",
                        strokeWidth: "2",
                        stroke: "#ffa726"
                    }
                    }}
                    style={{
                        borderRadius: 8,
                    }}
                />

                <Text style = {dataStyles.graphLabel}>Normalized Velocity</Text>
            </View>
            
            <View>
                <LineChart
                    data={{
                    labels: new Array(tempData.length),
                    datasets: [
                        {
                        data: tempData
                        }
                    ]
                    }}
                    width={Dimensions.get("window").width/3.5} // from react-native
                    height={220}
                    yAxisInterval={1} // optional, defaults to 1
                    chartConfig={{
                    backgroundGradientFrom: "#3370d6",
                    backgroundGradientTo: "#3370d6",
                    decimalPlaces: 2, // optional, defaults to 2dp
                    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    style: {
                        borderRadius: 16
                    },
                    propsForDots: {
                        r: "6",
                        strokeWidth: "2",
                        stroke: "#ffa726"
                    }
                    }}
                    style={{
                        borderRadius: 8,
                        marginHorizontal: 70
                    }}
                />

                <Text style = {dataStyles.graphLabel}>Temperature</Text>
            </View>
            
            <View>
                <LineChart
                    data={{
                    labels: new Array(altData.length),
                    datasets: [
                        {
                        data: altData
                        }
                    ]
                    }}
                    width={Dimensions.get("window").width/3.5} // from react-native
                    height={220}
                    yAxisInterval={1} // optional, defaults to 1
                    chartConfig={{
                    backgroundGradientFrom: "#3370d6",
                    backgroundGradientTo: "#3370d6",
                    decimalPlaces: 2, // optional, defaults to 2dp
                    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    style: {
                        borderRadius: 16
                    },
                    propsForDots: {
                        r: "6",
                        strokeWidth: "2",
                        stroke: "#ffa726"
                    }
                    }}
                    style={{
                        borderRadius: 8,
                        justifyContent: "center"
                    }}
                />

                <Text style = {dataStyles.graphLabel}>Altitude</Text>
            </View>

        </View>
    );
}

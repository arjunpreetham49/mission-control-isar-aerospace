import { Text, View, FlatList } from 'react-native';
import { useEffect, useState, useRef } from "react";
import { store } from '../../redux/apiRedux';
import dataStyles from '../../stylesheets/data-stylesheet';

export function LogUI(){
    const [messages, setMessages] = useState([]);
    const flatListRef = useRef("list");

    useEffect(() => {
        store.subscribe(() => {
            const state = store.getState(); 

            if(state !== null){
                bindDataToView(state.graphData);
            }
        });
    }, []);

    function bindDataToView(data){
        setMessages(data.statusMessage);
    }

    return(
        <View style = {dataStyles.listContainer}>
            <Text style = {dataStyles.logHeading}>Mission Log</Text>
            <FlatList
                    ref={flatListRef}
                    onContentSizeChange={() => {
                        setTimeout(() => {
                            flatListRef.current.scrollToEnd(); 
                          }, 70);
                    }}
                    style = {dataStyles.listStyle}
                    data={messages}
                    renderItem={({item}) => <Text style={dataStyles.logMessageStyle}>{item.timestamp} &gt; {item.message}</Text>}
                />
        </View>
    );
}
import {React, useEffect, useState } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import styles from './web/stylesheets/toolbar-stylesheet';
import dataStyles from './web/stylesheets/data-stylesheet';
import { toggleLiveTelemetry } from './api/webSocket';
import { store } from './web/redux/apiRedux';
import { setLiveMode } from './web/redux/apiRedux';
import {GraphSection} from './web/ui/dashboard/graph_section'
import { LogUI } from './web/ui/dashboard/log_ui';
import { ControlPanel } from './web/ui/dashboard/control_panel_ui';
import Alert from 'react-native-awesome-alerts';

function App() {
  const [isLive, setIsLive] = useState(true);
  const [changeInDirection, setDirectionChange] = useState(false);
  const [date, setDate] = useState(new Date().toISOString());
  const [spectrumData, setSpectrumData] = useState(null);

  useEffect(() => {
    //Get Latest Spectrum Status to update UI
    toggleLiveTelemetry(true);

    store.subscribe(() => {
      let state = store.getState(); 

      setSpectrumData(state.graphData);

      if(!state.graphData.prevDirection){
        setDirectionChange(true); 
      }

      
    });

    //Get Date 
    setInterval(() => {
      var date = new Date().toISOString();
      setDate(date);
    }, 1000);

  }, []);

  // Toggle Live Mode
  function toggleLiveMode(){
    setIsLive(!isLive);
    toggleLiveTelemetry(!isLive);

    //Update Redux to update UI
    store.dispatch(setLiveMode({liveMode: !isLive}));
  }

  return (
    <View style={styles.container}>

    {/* Toolbar */}
    <View style={styles.toolbar}>
      {/* Spectrum Mission Control style */}
      <View style={styles.toolbar_part}>
        <View style = {styles.mcStyle} >

        <Image 
          style = {styles.logo}
          source = {require('./assets/images/isar-logo.svg')} />

        <Text style = {styles.mcTextStyle}>Spectrum Mission Control</Text>

        </View>
      </View>

      <View style={styles.toolbar_part}>
        {/* UTC Timestamp */}

        <Text style={styles.utcTimeStamp}>{date}</Text>
        <Text style = {{color: "#FFFFFF", textAlign: "center"}}>UTC</Text>
      </View>

      <View style={styles.toolbar_part}>
        {/* Live Mode Section */}

        <View style = {styles.liveModeStyle}>
        <TouchableOpacity 
        onPress={toggleLiveMode}>
          <Text style = {isLive ? styles.liveModeTextStyleEnabled : styles.liveModeTextStyleDisabled}>Live Telemetery</Text>
        </TouchableOpacity>

        </View>
      </View>
    </View>

    {/* Data View */}
    <View style = {dataStyles.container}>
      {spectrumData !== null ? 
      (<Text style = {spectrumData.direction ? dataStyles.directionAscending : dataStyles.directionDescending}>{spectrumData.direction ? "Vehicle Ascending" : "Vehicle Descending"}</Text>) 
      : 
      (<Text style = {dataStyles.directionDescending}>No Status</Text>)
      }      

      <GraphSection></GraphSection>

      {/* Divider */}
      <View style={{backgroundColor: "#787878", width: "95%", height: 1, left: 10, right: 10, alignSelf: 'center', top: "8vh"}}></View>

      <View style={{flexDirection: 'row', backgroundColor: "#000000", top: "10vh", height: 350, paddingBottom: "5vh"}}>
      <LogUI></LogUI>

      <ControlPanel></ControlPanel>

      </View>
    </View>

    <Alert
          show={spectrumData !== null ? !spectrumData.direction && changeInDirection : false}
          message="SPECTRUM DESCENDING!"
          closeOnTouchOutside={true}
          onDismiss = {() => {
            setDirectionChange(false);
          }}
        />

    </View>

  );
}

export default App;


